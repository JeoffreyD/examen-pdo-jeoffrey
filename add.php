<?php

$options = [
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
];

try {

    $pdo = new PDO('mysql:host=localhost;dbname=exam_jeoffrey', 'root', '', $options);

    $query = 'SELECT * FROM genre';
    $results = $pdo->prepare($query);
    $results->execute();

    $rows = $results->fetchAll(PDO::FETCH_ASSOC);

} catch (Exception $e) {
    var_dump($e);
}

function dump($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ajouter un film</title>
    <link rel="stylesheet" href="assets/styles/index.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <div class="main">
        <form action="treat-form-add.php" method="post">
            <div class="my-flex">
                <div>
                    <label for="title">Titre du film</label>
                    <input type="text" required="required" class="form-control" name="title" id="title">
                </div>
                <div>
                    <label for="genre">Choisir un genre</label>
                    <select type="text" required="required" class="form-control" name="genre" id="genre">
                        <?php foreach ($rows as $row){ ?>
                            <option value="<?php echo $row['id'] ?>"><?php echo $row['label'] ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div>
                    <label for="launchedAt">Date de sortie</label>
                    <input type="date" required="required" class="form-control" name="launchedAt" id="launchedAt">
                </div>
                <div>
                    <label for="img">lien de l'image du film</label>
                    <input type="text" required="required" class="form-control" name="img" id="img">
                </div>
                <div>
                    <button type="submit">Ajouter un film</button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>