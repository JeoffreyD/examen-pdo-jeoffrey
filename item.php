<?php

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
);

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    try {
        $pdo = new PDO('mysql:host=localhost;dbname=exam_jeoffrey', 'root', '', $options);

        $query = 'SELECT * FROM movie JOIN genre ON movie.genre_id = genre.id WHERE movie.id = :param_id';
        $results = $pdo->prepare($query);
        $results->execute([
            ':param_id' => $id,
        ]);

        $row = $results->fetch(PDO::FETCH_ASSOC);

        $date = new DateTime($row['launched_at']);

    } catch (Exception $e) {
    }
} else {
    echo "je n'ai pas recu d'id !!!!";
}
function dump($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Détail du Film</title>
    <link rel="stylesheet" href="assets/styles/index.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body>
<div class="container">
    <div class="main">
        <div class="row">
            <div class="col-md-4 col-12">
                <div class="card" style="width: 18rem;">
                    <img src="<?php echo $row['image_link'] ?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $row['title'] ?></h5>
                        <p class="card-text">Genre : <?php echo $row['label'] ?></p>
                        <p class="card-text">Date de sortie : <?php echo $date->format('d/m/Y') ?></p>
                        <a href="index.php" class="btn btn-primary">Retourner à l'acceuil</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
