Bonjour et bienvenue sur l'examen de Jeoffrey (sûrement une personne très sympa)

Pour pouvoir initialiser le projet :

1 - Aller dans le queries.txt et exécuter les requêtes afin de créer la database (elles marchent alors ne me faite pas croire qu'elles ne marchent pas).

2 - Faire un "npm install" dans votre terminal.

3 - Faites un "php -S localhost:8000" pour lancer le serveur et rendez-vous sur l'URL générée dans le terminal.

4 - Dans le cas (sûrement pas le cas) où le css ne marcherait pas, faire un "npm run sass" dans votre terminal.


Si vous avez des problèmes n'hésitez pas à les régler vous-même ou venir me voir si c'est urgent