<?php
include 'pdo.php';
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exam PDO Jeoffrey</title>
    <link rel="stylesheet" href="assets/styles/index.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="my-btn mt-3">
        <a href="add.php" class="btn btn-primary">Ajouter un film</a>
        <form action="" method="get">
            <input type="text" name="search" id="search" placeholder="Rechercher un film">
            <button type="submit" class="btn btn-primary">Rechercher</button>
        </form>
    </div>

    <div class="row my-3">
        <?php foreach ($rows as $row) { ?>
            <div class="col-md-3 col-12 mb-4">
                <div class="card">
                    <img src="<?php echo $row['image_link'] ?>" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><?php echo $row['title'] ?></h5>
                        <p class="card-text">J'aurais été une super description si Jules avait demandé une description de film</p>
                        <a href="item.php?id=<?php echo $row['id'] ?>" class="btn btn-primary">Détail du Film</a>
                        <a href="delete-movie.php?id=<?php echo $row['id'] ?>" class="btn btn-primary mt-2">Supprimer le
                            Film</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
</body>
</html>
