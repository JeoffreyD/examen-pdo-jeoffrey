<?php

$options = [
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
];

try {

    $pdo = new PDO('mysql:host=localhost;dbname=exam_jeoffrey', 'root', '', $options);

    if (isset($_GET['search'])){
        $search = $_GET['search'];
        $query = "SELECT * FROM movie WHERE title LIKE '%' :param_search '%'";
        $results = $pdo->prepare($query);
        $results->execute([
            ':param_search' => $search
        ]);
    } else {
        $query = 'SELECT * FROM movie';
        $results = $pdo->prepare($query);
        $results->execute();
    }


    $rows = $results->fetchAll(PDO::FETCH_ASSOC);


} catch (Exception $e) {
    var_dump($e);
}

function dump($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}

?>