<?php

$options = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
);
if (
    isset($_POST['title']) &&
    isset($_POST['genre']) &&
    isset($_POST['launchedAt']) &&
    isset($_POST['img'])
) {
    $title = $_POST['title'];
    $genre = $_POST['genre'];
    $launchedAt = $_POST['launchedAt'];
    $img = $_POST['img'];

    try {
        $pdo = new PDO('mysql:host=localhost;dbname=exam_jeoffrey', 'root', '', $options);

        $query =
            'INSERT INTO movie (title, launched_at, image_link, genre_id) VALUES (:param_title, :param_launchedAt, :param_image, :param_genre)';
        $resultats = $pdo->prepare($query);
        $resultats->execute([
            ':param_title' => $title,
            ':param_genre' => $genre,
            ':param_launchedAt' => $launchedAt,
            ':param_image' => $img,
        ]);

        header('location: index.php');

    } catch (Exception $e) {
        var_dump($e);
    }
}
function dump($data)
{
    echo '<pre>';
    var_dump($data);
    echo '</pre>';
}
?>